package mpr.oth;
import java.net.*;

public class TimeOut implements Runnable{

	Socket s,s1;
	public TimeOut(Socket s,Socket s1){
		this.s=s;
		this.s1=s1;
	}
	
	@Override
	public void run() {
		try {
				Thread.sleep(15000);
				s.close();
				s1.close();
				System.out.println("\n>>>>>>>>> Connection timed out");
			} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}
	
}
