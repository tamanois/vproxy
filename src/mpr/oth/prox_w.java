package mpr.oth;
import java.io.*;
import java.net.Socket;
import java.text.DecimalFormat;

public class prox_w implements Runnable{
	private BufferedInputStream bf;
	private BufferedOutputStream writer;
	private Socket s;

	public prox_w(BufferedInputStream bf,BufferedOutputStream writer, Socket s ){
		this.bf=bf;
		this.writer=writer;
		this.s=s;
	}
	@Override
	public void run() {
		long byteCount=0;
		try{
			
				
				try {
					
					byte[] buf = new byte[8192];
					@SuppressWarnings("unused")
					int bytesIn = 0;
					while (((bytesIn = bf.read(buf)) >= 0) )
					{
						//System.out.println("[Bytes sended "+" : " + bytesIn+"]");
						writer.write(buf, 0, bytesIn);
						writer.flush();
						byteCount += bytesIn;
					}

				}  catch (Exception e)  {}
				finally {
					try {
						bf.close();
					} catch (Exception e2) {
						// TODO: handle exception
					}
					try {
						writer.close();
					} catch (Exception e2) {
						// TODO: handle exception
					}
					
					
					String value= new DecimalFormat("#.##").format((double)((double)byteCount/(double)1024));
					System.out.println("[Bytes sended "+" : " + value +" Ko ] by "+s.getInetAddress().getHostAddress());
					try {
						s.close();
					} catch (Exception e2) {
						// TODO: handle exception
					}

				}
				
				
				
			
		}catch(Exception e){
			//System.out.println("class prox: ");
			//e.printStackTrace();
			}
		
	}
}
