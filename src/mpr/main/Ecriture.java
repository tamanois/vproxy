package mpr.main;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.text.DecimalFormat;


import mpr.main.Ecoute;
import mpr.main.tray;
import mpr.ssl.proxy;


public class Ecriture implements Runnable{
	private Socket s;
	  int limit=0;
	public static	final String authString = "Proxy-Authorization:"; 


	
	public Ecriture(Socket s){
		this.s=s;
	}
	@Override
	public void run() {
		Socket sock=null;
		boolean startwC=false;
		String ferme="",host="",query="";
		StringBuffer request = new StringBuffer("");
		ByteArrayOutputStream bs = new ByteArrayOutputStream();
		BufferedInputStream bf=null,reader=null;
		BufferedOutputStream pw=null,writer=null;
		boolean mustProceed = true;
		boolean authFinded = false;
		String port="";
		String Itoken="none";

		try{
			bf= new BufferedInputStream(s.getInputStream());
			
			pw =  new BufferedOutputStream(s.getOutputStream());
			int a=-1;
			int cl=0;
			
			
			while( ((ferme = readLine1(bf))!= null)) {
				
				if(tray.proxyAuth) {
					if(ferme.toLowerCase().contains(authString.toLowerCase())) {
						String b64Auth = ferme.substring(authString.length()).trim();
						authFinded = true;

						if (!Ecoute.checkAuth(b64Auth)) {
							mustProceed = false;
							break;
						}
						
					}
				}
				
				if (ferme.length()==0) {
					if (query.startsWith("POST")){
						ferme=readLine1(bf,cl);
						query=query+"\r\n"+ferme+"\r\n";
					}
					break;}
				query=query+ferme+"\r\n";
				a = ferme.toLowerCase().indexOf("content-length:");
				if (a >= 0)
					cl = Integer.parseInt(ferme.substring(a + 15).trim());
				
				
			}
			
			
				if(tray.verbose)
					System.out.println(query);
//				tray.pw.println(query);
//				tray.pw.flush();
		}catch(SocketTimeoutException ste) {
			try {
				s.close();
			} catch (Exception e) {
				// TODO: handle exception
			}
			}catch(Exception e){}
		 finally {
			 if(tray.proxyAuth && !authFinded) {
				 mustProceed = false;
			 }
		 }
		
		if (mustProceed){
			int iii=query.indexOf("x-melinois-limit");
			if(iii>=0){
				
				String RealLimit=query.substring(iii+18,query.indexOf("@xml-end"));
				try {
					limit=Integer.parseInt(RealLimit);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					limit=0;
					e.printStackTrace();
				}

			}

			int ii=query.indexOf("x-melinois-inject");
			if(ii>=0){
				
				String RealEndPoint=query.substring(ii+19,query.indexOf("@xmi-end"));
				port = RealEndPoint.substring(RealEndPoint.indexOf(':')+1);
				host=RealEndPoint.substring(0,RealEndPoint.indexOf(':'));
				ii=query.indexOf("x-melinois-token");
				if(ii>=0){
				Itoken=query.substring(ii+18,query.indexOf("@xmt-end"));
				}
				startwC=true;


			}else{
				
				if(tray.mtwb==0){
					try {
						s.close();
					} catch (IOException e) {
						
					}
					return;
				}
			
				
				 if (query.startsWith("GET")){
					String sub = query.substring(11);
					host = sub.substring(0,sub.indexOf("HTTP/")-1);
					port="80";
					/*query= query.replaceFirst(host, "");
					query= query.replaceFirst("http://", "");
					query= query.replaceAll("HTTP/1.1", "HTTP/1.1");
					query = query.substring(0,3)+' '+query.substring(query.indexOf('/'));*/
					query= query+"\r\n";
				}
				else if (query.startsWith("POST")){
					String sub = query.substring(12);
					host = sub.substring(0,sub.indexOf("HTTP/")-1);
					port="80";
					/*query= query.replaceAll("HTTP/1.1", "HTTP/1.1");
					query= query.replaceFirst("http://", "");
					query = query.substring(0,4)+' '+query.substring(query.indexOf('/'));*/
					
				}
				else if (query.startsWith("CONNECT")){
					String sub = query.substring(8);
					port = sub.substring(sub.indexOf(':')+1,sub.indexOf("HTTP")-1);
					host = query.substring(query.indexOf(" ")+1, query.indexOf(":")); //host = sub.substring(0,sub.indexOf(':'));
					query= query.replaceFirst("CONNECT", "GET");
					query= query.replaceFirst("HTTP/1.1", "HTTP/1.1");
					query= query.substring(0,3)+" / "+ query.substring(query.indexOf("HTTP/1"));
					//query= query.substring(0,query.indexOf("\n")+1);
					query= query+"\r\n";
					startwC=true;
				}
			}
			
			
			request.append(query);
			bs.write(request.toString().getBytes(), 0, request.length());
			//System.out.print(request.toString());
	        int pt = Ecoute.toInt(port);
			host = host.replaceAll("/", "");

			System.out.println(host+':'+pt+'\n');
//			tray.pw.println(host+':'+pt+'\n');
//			tray.pw.flush();
			
			try{
				if (startwC) {
					//System.out.println(host+':'+pt+'\n');
					proxy pr= new proxy(s,host,pt,Itoken,limit);
					Thread tp= new Thread(pr);
					tp.start();}
			 else{
				
					 sock = new Socket(host,pt);
					 System.out.println("Connecte a "+host);
//					 tray.pw.println("Connecte a "+host);
//					 tray.pw.flush();
					 reader = new BufferedInputStream(sock.getInputStream());
					
					 writer=new BufferedOutputStream(sock.getOutputStream());
									 
			
					
				
				
					
					writer.write(bs.toByteArray(), 0, bs.toByteArray().length);
					writer.flush();
					
					//Sender thread
					mpr.oth.prox_w p = new mpr.oth.prox_w(bf,writer,s);
					Thread Tprox =new Thread(p);
					Tprox.start();
					
				
				//receiver thread
	              
					
				final BufferedInputStream myReader=reader;
				final BufferedOutputStream myPw= pw;
				final String myHost=host;
				final Socket mySock=sock;
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						long byteCount=0;

						try {
							
							byte[] buf = new byte[8192];
							int bytesIn = 0;
							while (((bytesIn = myReader.read(buf)) >= 0) )
							{
								//System.out.println("[Bytes received "+" : " + bytesIn+"]");
								myPw.write(buf, 0, bytesIn);
								myPw.flush();
								byteCount += bytesIn;
							}

						}  catch (Exception e)  {}
						finally {
							try {
								myReader.close();
							} catch (Exception e2) {
								// TODO: handle exception
							}
							try {
								myPw.close();
							} catch (Exception e2) {
								// TODO: handle exception
							}
							try {
								mySock.close();
							}catch(Exception e2) {}
							String value= new DecimalFormat("#.##").format((double)((double)byteCount/(double)1024));
							System.out.println("[Bytes received "+" : " + value +" Ko ] by "+myHost);

						}
					}
				}).start();
					
			}
			}catch(Exception e) {
				e.printStackTrace();
				try {
					s.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}	
		else {
			try {
				s.close();
			} catch (Exception e) {
			}
		}
	}
	public static String readLine (InputStream in)
	{
		// reads a line of text from an InputStream
		StringBuffer data = new StringBuffer("");
		int c;
		
		try
		{
			// if we have nothing to read, just return null
			in.mark(1);
			try {
				if (in.read() == -1)
	
					return null;
				else
					in.reset();
			}catch(Exception e){}
			while ((c = in.read()) >= 0)
			{
				// check for an end-of-line character
				if ((c == 0) || (c == 10) || (c == 13))
					break;
				else
					data.append((char)c);
			}
		
			// deal with the case where the end-of-line terminator is \r\n
			if (c == 13)
			{
				in.mark(1);
				if (in.read() != 10)
					in.reset();
			}
		}  catch (Exception e)  {
			e.printStackTrace();
		}
		
		// and return what we have
		return data.toString();
	}
	
 public static String readLine1(InputStream in) throws IOException{
	 String line="";
	 int c;
	 while ((c=in.read())>=0){
		 if (((char)c=='\n')||((char)c=='\r')){in.read(); break;}
		 line += (char)c;
		 //System.out.print((char)c);
	 }
	 //System.out.print("fini: "+line.length()+"\n");
	 return line;
 }
 public static String readLine1(InputStream in,int nb) throws IOException{
	 String line="";
	 int c;
	 while ((nb>0)){
		 c=in.read();
		 line += (char)c;
		 
		 nb--;
	 }
	 return line;
 }
}


